﻿using CarFinance247.Models;
using Microsoft.AspNetCore.Mvc;

namespace CarFinance247.Controllers
{
    [Route("api/v1/customer")]
    public class CustomerController : Controller
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [HttpPost("add")]
        public string Add([FromBody] Customer customer)
        {
            return _customerRepository.Add(customer);
        }

        [HttpGet("remove")]
        public string Remove(string id)
        {
            return _customerRepository.Remove(id);
        }

        [HttpGet("get")]
        public Customer Get(string id)
        {
            return _customerRepository.Get(id);
        }

        [HttpPost("update")]
        public string Update([FromBody]Customer customer)
        {
            return _customerRepository.Update(customer);
        }
    }
}
