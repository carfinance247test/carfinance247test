namespace CarFinance247.Models
{
    public interface ICustomerRepository
    {
        string Add(Customer customer);
        string Remove(string id);
        Customer Get(string id);
        string Update(Customer customer);
    }
}