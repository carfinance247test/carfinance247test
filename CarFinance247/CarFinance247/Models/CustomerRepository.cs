﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CarFinance247.Models
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly List<Customer> _customers;

        public CustomerRepository()
        {
            _customers = new List<Customer>();
        }

        public string Add(Customer customer)
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                _customers.Add(
                    new Customer()
                    {
                        Id = id,
                        FirstName = customer.FirstName,
                        Surname = customer.Surname,
                        EmailAddress = customer.EmailAddress,
                        Password = customer.Password
                    });
                return id;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "Add unsuccessful";
            }
        }

        public string Remove(string id)
        {
            try
            {
                var toRemove = _customers.FirstOrDefault(x => x.Id == id);
                var removed = _customers.Remove(toRemove);
                return $"Delete {(removed ? "successful" : "unsuccessful")}";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "Remove unsuccessful";
            }
        }

        public Customer Get(string id)
        {
            try
            {
                return _customers.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public string Update(Customer customer)
        {
            try
            {
                for (var i = 0; i < _customers.Count; i++)
                {
                    var c = _customers.ElementAt(i);
                    if (!c.Id.EndsWith(customer.Id)) continue;
                    _customers[i] = customer;
                    return "Update successful";
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return "Update unsuccessful";
        }
    }
}
